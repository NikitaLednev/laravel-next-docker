# Laravel + Next.js docker

This is a docker-composite image for Next.js projects in conjunction with Laravel.

## Installation

```bash
# build image
docker-compose build

# run image
docker-compose up

# stop image
docker-compose down

```


## Usage
### About environment variables
For Next.js environment use nginx container name as host:

```bash
# next.js .env file

SITE_URL=http://nginx

```

To connect database in Laravel use mysql container name:

```bash
# laravel .env file

DB_HOST=db

```

### About working with environments
Any interaction with the environment is carried out through the container:

```bash
# Execute an interactive bash shell on the container

docker exec -it <container_name> bash

```
This will create a new Bash session in the container

### Local urls
Don't forget to add the host urls to the hosts file